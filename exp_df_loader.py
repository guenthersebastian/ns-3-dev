import numpy as np
import pandas as pd
import pickle


DEFAULT_RESULT_DIR = "/home/seb/repos/ns-3-dev/results_remote/"


def get_arrivalrate_set(result_dir=DEFAULT_RESULT_DIR):
    df = pd.DataFrame(columns=("arrivalRate", "size", "goodput", "drops", "delayAvg", "delayMin", "delayMax"))
    for arrival_intv in [8000, 10000, 15000, 20000, 22000, 22350, 22700, 25000, 35000, 50000]:
        for mpdu_size in [100, 200, 500, 1000, 1500]:
            for run in range(1, 16, 1):
                result_filename = result_dir + "dist11.000_size%d_max64_rts0_dur5.0000_intv%d_1_%d_results.pkl" % (mpdu_size, arrival_intv, run)
                try:
                    sim_results_file = open(result_filename, "rb")
                    sim_results = pickle.load(sim_results_file)
                    sim_results_file.close()
                    arrival_rate = 1.0 / (arrival_intv / 1000000000)
                    df.loc[len(df.index)] = [arrival_rate, mpdu_size, sim_results['throughput'], sim_results['mdpuDropCount'], sim_results['delayAvg'], sim_results['delayMin'], sim_results['delayMax']]
                    print("Loaded: %s" % result_filename)
                except IOError:
                    print("Does not exist: %s" % result_filename)
    return df
