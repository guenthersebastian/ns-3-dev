import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


# creates a row for Sheets from the given dictionaries
def create_row(params_dict, results_dict, stats_dict):
    row = ([params_dict['seed'], params_dict['run'], params_dict['simDuration'], params_dict['packetArrivalRate'], 
    params_dict['maxMpduCount'], params_dict['distance'], params_dict['mpduSize'], params_dict['rtsEnabled']])
    row.append(results_dict['throughput']) if 'throughput' in results_dict else row.append("")
    row.append(results_dict['perAvg']) if 'perAvg' in results_dict else row.append("")
    row.append(results_dict['perStdDev']) if 'perStdDev' in results_dict else row.append("")
    row.append(results_dict['delayAvg']) if 'delayAvg' in results_dict else row.append("")
    row.append(results_dict['delayStdDev']) if 'delayStdDev' in results_dict else row.append("")
    row.append(results_dict['delayMin']) if 'delayMin' in results_dict else row.append("")
    row.append(results_dict['delayMax']) if 'delayMax' in results_dict else row.append("")
    row.append(results_dict['aggregationCountAvg']) if 'aggregationCountAvg' in results_dict else row.append("")
    row.append(results_dict['aggregationCountStdDev']) if 'aggregationCountStdDev' in results_dict else row.append("")
    row.append(results_dict['aggregationCountMin']) if 'aggregationCountMin' in results_dict else row.append("")
    row.append(results_dict['aggregationCountMax']) if 'aggregationCountMax' in results_dict else row.append("")
    row.append(results_dict['mdpuCount']) if 'mdpuCount' in results_dict else row.append("")
    row.append(results_dict['mdpuDropCount']) if 'mdpuDropCount' in results_dict else row.append("")
    row.append(stats_dict['start']) if 'start' in stats_dict else row.append("")
    row.append(stats_dict['duration']) if 'duration' in stats_dict else row.append("")
    return row


# uploads the given set of rows to Sheets
def upload_rows(rows):
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
    NS3_SPREADSHEET_ID = '1RQEinDf6_yNk9DWG4Dq3W2LFgjmDbvYlfb3vcCQ3R28'
    NS3_DATA_RANGE_NAME = 'runs!A2:W'

    # authentication
    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    
    # log in if necessary
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    
    # upload
    service = build('sheets', 'v4', credentials=creds)
    body = {'values': rows}
    result = service.spreadsheets().values().append(spreadsheetId=NS3_SPREADSHEET_ID, range=NS3_DATA_RANGE_NAME, valueInputOption="RAW", body=body).execute()
    return result.get('updates').get('updatedCells')
