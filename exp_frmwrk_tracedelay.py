import re
import statistics
import collections


# parses the log file and extracts trace delay stats
def parse_log_file(log_file_name):
    print("PARSING log file: %s" % log_file_name)

    pattern_tracedelay = re.compile('\+([0-9\.]*)s TraceDelay: RX ([0-9]+) bytes from 192.168.1.2 Sequence Number: ([0-9]+) Uid: ([0-9]+) TXtime: \+([0-9]+).0ns RXtime: \+([0-9]+).0ns Delay: \+([0-9]+).0ns')

    # track when sequence numbers are skipped
    skips = []

    # track all delays
    delays = []

    # total packets received
    total_packets = 0

    # Last sequence number received
    last_sequence_number = -1

    # parse line by line
    with open(log_file_name) as log_file:
        for line in log_file:
            # always check frame error rates
            match = pattern_tracedelay.search(line.strip())
            if match != None:
                sequence_number = int(match.group(3))
                delay = int(match.group(7))
                
                # collect stats about how many packets are received
                total_packets += 1

                # check if we skipped any frames
                skipped = sequence_number - last_sequence_number - 1
                if skipped > 0:
                    skips.append(skipped)
                last_sequence_number = sequence_number

                # save the delay for the current packet
                delays.append(delay)

    # calculate statistics
    trace_results = {}
    trace_results['totalReceived'] = total_packets
    trace_results['totalSkipped'] = sum(skips)
    try:
        trace_results['delayAvg'] = statistics.mean(delays)
        trace_results['delayStdDev'] = statistics.stdev(delays)
        trace_results['delayMin'] = min(delays)
        trace_results['delayMax'] = max(delays)
    except:
        trace_results['delayAvg'] = 0.0
        trace_results['delayStdDev'] = 0.0
        trace_results['delayMin'] = 0
        trace_results['delayMax'] = 0
    
    return trace_results


if __name__ == '__main__':
    print(parse_log_file("sample_log.txt"))
