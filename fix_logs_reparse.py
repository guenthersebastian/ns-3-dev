#!/usr/bin/env python3

import exp_mgmt
import sys
import os
import re


REPARSE_DIR = "/home/seb/repos/ns-3-dev/results_paper/"
FILENAME_PATTERN = re.compile("dist([0-9.]+)_size([0-9]+)_max([0-9]+)_rts([01]{1})_dur([0-9.]+)_intv([0-9]+)_([0-9]+)_([0-9]+)_log.txt")

file_list = os.listdir(REPARSE_DIR)
i = 0
for file in file_list:
    i += 1
    if file.endswith("_log.txt"):
        print("==== [ %d of %d ] ================================" % (i, len(file_list)))
        # gather info
        file_name = os.path.join(REPARSE_DIR, file)
        search_mpdu_size = FILENAME_PATTERN.search(str(file).strip())
        if search_mpdu_size == None:
            print("Unable to infer simulation setup for %s" % str(file))
        else:
            params_dict = {}
            params_dict['distance'] = float(search_mpdu_size.group(1))
            params_dict['mpduSize'] = int(search_mpdu_size.group(2))
            params_dict['maxMpduCount'] = int(search_mpdu_size.group(3))
            params_dict['rtsEnabled'] = int(search_mpdu_size.group(4))
            params_dict['simDuration'] = float(search_mpdu_size.group(5))
            params_dict['packetArrivalRate'] = float(search_mpdu_size.group(6))
            params_dict['seed'] = int(search_mpdu_size.group(7))
            params_dict['run'] = int(search_mpdu_size.group(8))

            # Re-parse
            exp_mgmt.aggregate_results(params_dict, log_dir=REPARSE_DIR)

            # recreate files if missing
            run_name = exp_mgmt.get_run_name(params_dict)
            if not os.path.isfile(os.path.join(REPARSE_DIR, run_name + "_params.pkl")):
                print("RECREATING params file")
                exp_mgmt.save_params_file(params_dict, REPARSE_DIR)
            if not os.path.isfile(os.path.join(REPARSE_DIR, run_name + "_stats.pkl")):
                print("RECREATING stats file")
                exp_mgmt.save_var_to_file(os.path.join(REPARSE_DIR, run_name + "_stats.pkl"), {"start":0,"duration":0})
