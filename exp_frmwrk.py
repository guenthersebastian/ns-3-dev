import exp_mgmt
import sys
import os
import re


DEFAULT_RESULTS_DIR = "/home/seb/exp/results/"
FILENAME_TEMPLATE = "dist([0-9.]+)_size([0-9]+)_max([0-9]+)_rts([01]{1})_dur([0-9.]+)_intv([0-9]+)_([0-9]+)_([0-9]+)"
RESULT_FILENAME_PATTERN = re.compile(FILENAME_TEMPLATE + "_(log\.txt|out\.txt|params\.pkl|results\.pkl|stats\.pkl)")
LOG_FILENAME_PATTERN = re.compile(FILENAME_TEMPLATE + "_log.txt")


# Returns all files that end with _log.txt in the given directory
def get_all_log_files(results_dir=DEFAULT_RESULTS_DIR):
    log_files = []
    for file in os.listdir(results_dir):
        if file.endswith("_log.txt"):
            log_files.append(os.path.join(results_dir, file))
    return log_files


# Returns all files that end with _results.pkl in the given directory
def get_all_pkl_sets(results_dir=DEFAULT_RESULTS_DIR):
    result_sets = []
    for file in os.listdir(results_dir):
        # only check one of the mandatory types to avoid duplicates
        if file.endswith("_results.pkl"):
            result_set = get_filename_set(str(file))
            if result_set != None:
                params_path = os.path.join(results_dir, result_set[2])
                results_path = os.path.join(results_dir, result_set[3])
                stats_path = os.path.join(results_dir, result_set[4])
                if os.path.isfile(params_path) and os.path.isfile(results_path) and os.path.isfile(stats_path):
                    result_sets.append((params_path, results_path, stats_path))
    return result_sets


# Returns a tuple with all 5 filenames
def get_filename_set(filename):
    search_params = RESULT_FILENAME_PATTERN.search(str(filename).strip())
    if search_params == None:
        return None
    else:
        file_type = search_params.group(9)
        raw_filename = str(filename).replace(file_type, "")
        return (raw_filename + "out.txt", raw_filename + "log.txt", raw_filename + "params.pkl", raw_filename + "results.pkl", raw_filename + "stats.pkl")


# Tries to infer simulation parameters from the filename given
def get_params_from_log_filename(filename):
    search_params = LOG_FILENAME_PATTERN.search(str(filename).strip())
    if search_params == None:
        return None
    else:
        params_dict = {}
        params_dict['distance'] = float(search_params.group(1))
        params_dict['mpduSize'] = int(search_params.group(2))
        params_dict['maxMpduCount'] = int(search_params.group(3))
        params_dict['rtsEnabled'] = int(search_params.group(4))
        params_dict['simDuration'] = float(search_params.group(5))
        params_dict['packetArrivalRate'] = float(search_params.group(6))
        params_dict['seed'] = int(search_params.group(7))
        params_dict['run'] = int(search_params.group(8))
        return params_dict