#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle


distances = [0.0, 4.0, 8.0, 9.0, 9.2, 9.4, 9.6, 9.8, 10.0, 10.2, 10.4, 10.6, 10.8, 11.0, 11.2, 11.4]

df_errorrate_goodput = pd.DataFrame(columns=("distance", "size", "goodput"))

for distance in distances:
    for mpdu_size in [100, 1000, 1500]:
        for run in range(1, 6, 1):
            result_filename = "/home/seb/repos/ns-3-dev/results_remote/dist%.03f_size%d_max64_rts0_dur5.0000_intv100_1_%d_results.pkl" % (distance, mpdu_size, run)
            try:
                sim_results_file = open(result_filename, "rb")
                sim_results = pickle.load(sim_results_file)
                sim_results_file.close()
                df_errorrate_goodput.loc[len(df_errorrate_goodput.index)] = [distance, 'off%d' % mpdu_size, sim_results['throughput']]
                print("Loaded: %s" % result_filename)
            except IOError:
                print("Does not exist: %s" % result_filename)

for distance in distances:
    for mpdu_size in [100, 1000, 1500]:
        for run in range(1, 6, 1):
            result_filename = "/home/seb/repos/ns-3-dev/results_remote/dist%.03f_size%d_max64_rts1_dur5.0000_intv100_1_%d_results.pkl" % (distance, mpdu_size, run)
            try:
                sim_results_file = open(result_filename, "rb")
                sim_results = pickle.load(sim_results_file)
                sim_results_file.close()
                df_errorrate_goodput.loc[len(df_errorrate_goodput.index)] = [distance, 'on%d' % mpdu_size, sim_results['throughput']]
                print("Loaded: %s" % result_filename)
            except IOError:
                print("Does not exist: %s" % result_filename)

print(df_errorrate_goodput.to_string())

# get mean and standard distribution
mean_errorrate_goodput_100 = df_errorrate_goodput.where(df_errorrate_goodput['size']=='off100').groupby('distance')['goodput'].mean()
std_errorrate_goodput_100 = df_errorrate_goodput.where(df_errorrate_goodput['size']=='off100').groupby('distance')['goodput'].std()
mean_errorrate_goodput_1000 = df_errorrate_goodput.where(df_errorrate_goodput['size']=='off1000').groupby('distance')['goodput'].mean()
std_errorrate_goodput_1000 = df_errorrate_goodput.where(df_errorrate_goodput['size']=='off1000').groupby('distance')['goodput'].std()
mean_errorrate_goodput_1500 = df_errorrate_goodput.where(df_errorrate_goodput['size']=='off1500').groupby('distance')['goodput'].mean()
std_errorrate_goodput_1500 = df_errorrate_goodput.where(df_errorrate_goodput['size']=='off1500').groupby('distance')['goodput'].std()
mean_errorrate_goodput_100_on = df_errorrate_goodput.where(df_errorrate_goodput['size']=='on100').groupby('distance')['goodput'].mean()
std_errorrate_goodput_100_on = df_errorrate_goodput.where(df_errorrate_goodput['size']=='on100').groupby('distance')['goodput'].std()
mean_errorrate_goodput_1000_on = df_errorrate_goodput.where(df_errorrate_goodput['size']=='on1000').groupby('distance')['goodput'].mean()
std_errorrate_goodput_1000_on = df_errorrate_goodput.where(df_errorrate_goodput['size']=='on1000').groupby('distance')['goodput'].std()
mean_errorrate_goodput_1500_on = df_errorrate_goodput.where(df_errorrate_goodput['size']=='on1500').groupby('distance')['goodput'].mean()
std_errorrate_goodput_1500_on = df_errorrate_goodput.where(df_errorrate_goodput['size']=='on1500').groupby('distance')['goodput'].std()

# plot it
plt.figure(figsize=(10.0, 5.0))
plt.errorbar(mean_errorrate_goodput_100.index, mean_errorrate_goodput_100, xerr=None, yerr=2*std_errorrate_goodput_100, linestyle=':', fmt='.b', markersize=1, barsabove=True, capsize=2, ecolor="b")
plt.errorbar(mean_errorrate_goodput_1000.index, mean_errorrate_goodput_1000, xerr=None, yerr=2*std_errorrate_goodput_1000, linestyle=':', fmt='.r', markersize=1, barsabove=True, capsize=2, ecolor="r")
plt.errorbar(mean_errorrate_goodput_1500.index, mean_errorrate_goodput_1500, xerr=None, yerr=2*std_errorrate_goodput_1500, linestyle=':', fmt='.g', markersize=1, barsabove=True, capsize=2, ecolor="g")
plt.errorbar(mean_errorrate_goodput_100_on.index, mean_errorrate_goodput_100_on, xerr=None, yerr=2*std_errorrate_goodput_100_on, linestyle=':', fmt='.c', markersize=1, barsabove=True, capsize=2, ecolor="c")
plt.errorbar(mean_errorrate_goodput_1000_on.index, mean_errorrate_goodput_1000_on, xerr=None, yerr=2*std_errorrate_goodput_1000_on, linestyle=':', fmt='.m', markersize=1, barsabove=True, capsize=2, ecolor="m")
plt.errorbar(mean_errorrate_goodput_1500_on.index, mean_errorrate_goodput_1500_on, xerr=None, yerr=2*std_errorrate_goodput_1500_on, linestyle=':', fmt='.y', markersize=1, barsabove=True, capsize=2, ecolor="y")
plt.legend(labels=("MPDU size 100B, RTS/CTS off", "MPDU size 1000B, RTS/CTS off", "MPDU size 1500B, RTS/CTS off",
                   "MPDU size 100B, RTS/CTS on", "MPDU size 1000B, RTS/CTS on", "MPDU size 1500B, RTS/CTS on"), loc=3, fancybox=False, edgecolor="k")
plt.xlabel('Distance (m)')
plt.xticks(np.arange(0.0, 11.6, 0.5))
plt.xlim([-0.1, 11.6])
plt.ylabel('Goodput (Mbit/s)')
#plt.ylim([0, 1300000000])
plt.savefig("img_paper_errorrate-goodput-rtscts.png", format="png", dpi=300)
if __name__ == '__main__':
    plt.show()
