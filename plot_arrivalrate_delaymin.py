#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle
import os

import exp_df_loader


# define the fields
X_AXIS_FIELD = 'arrivalRate'
Y_AXIS_FIELD = 'delayMin'
X_LABEL = 'Arrival rate (packets/s)'
Y_LABEL = 'Minimum delay (s)'
IMG_FILE_NAME = "img_paper_%s_%s.png" % (X_AXIS_FIELD, Y_AXIS_FIELD)


# load data set
df = exp_df_loader.get_arrivalrate_set("/home/seb/repos/ns-3-dev/results_remote/")
print(df.to_string())

# get mean and standard distribution 0 3 4 5 6 7 8 11
mean_100 = df.where(df['size']==100).groupby(X_AXIS_FIELD)[Y_AXIS_FIELD].mean()
std_100 = df.where(df['size']==100).groupby(X_AXIS_FIELD)[Y_AXIS_FIELD].std()
mean_200 = df.where(df['size']==200).groupby(X_AXIS_FIELD)[Y_AXIS_FIELD].mean()
std_200 = df.where(df['size']==200).groupby(X_AXIS_FIELD)[Y_AXIS_FIELD].std()
mean_500 = df.where(df['size']==500).groupby(X_AXIS_FIELD)[Y_AXIS_FIELD].mean()
std_500 = df.where(df['size']==500).groupby(X_AXIS_FIELD)[Y_AXIS_FIELD].std()
mean_1000 = df.where(df['size']==1000).groupby(X_AXIS_FIELD)[Y_AXIS_FIELD].mean()
std_1000 = df.where(df['size']==1000).groupby(X_AXIS_FIELD)[Y_AXIS_FIELD].std()
mean_1500 = df.where(df['size']==1500).groupby(X_AXIS_FIELD)[Y_AXIS_FIELD].mean()
std_1500 = df.where(df['size']==1500).groupby(X_AXIS_FIELD)[Y_AXIS_FIELD].std()

# plot it
plt.figure(figsize=(10.0, 5.0))
plt.errorbar(mean_100.index, mean_100, xerr=None, yerr=2*std_100, linestyle=':', fmt='.b', markersize=1, barsabove=True, capsize=2, ecolor="b")
plt.errorbar(mean_200.index, mean_200, xerr=None, yerr=2*std_200, linestyle=':', fmt='.r', markersize=1, barsabove=True, capsize=2, ecolor="r")
plt.errorbar(mean_500.index, mean_500, xerr=None, yerr=2*std_500, linestyle=':', fmt='.g', markersize=1, barsabove=True, capsize=2, ecolor="g")
plt.errorbar(mean_1000.index, mean_1000, xerr=None, yerr=2*std_1000, linestyle=':', fmt='.c', markersize=1, barsabove=True, capsize=2, ecolor="c")
plt.errorbar(mean_1500.index, mean_1500, xerr=None, yerr=2*std_1500, linestyle=':', fmt='.m', markersize=1, barsabove=True, capsize=2, ecolor="m")
plt.legend(labels=("MPDU size 100B", "MPDU size 200B", "MPDU size 500B", "MPDU size 1000B", "MPDU size 1500B"), loc=1, fancybox=False, edgecolor="k")
plt.xlabel(X_LABEL)
plt.ylabel(Y_LABEL)
#plt.xticks(np.arange(0.0, 51.0, 5.0))
#plt.xlim([-1.0, 51.0])
#plt.ylim([0, 1300000000])
plt.savefig(IMG_FILE_NAME, format="png", dpi=300)
if __name__ == '__main__':
    plt.show()
