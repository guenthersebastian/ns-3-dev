#!/usr/bin/env python3

import exp_log_parser
import sys


if len(sys.argv) != 3:
    print("Usage: ./test_log_parser.py [mpdu_size] [log_file]")
    quit(-1)

params_dict = {"mpduSize":int(sys.argv[1])}
print(exp_log_parser.process_log_file(str(sys.argv[2]), params_dict))