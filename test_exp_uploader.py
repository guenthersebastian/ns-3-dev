import exp_uploader

PERSISTENT_RESULTS_DIR = "/home/seb/exp/results/"

#params_dict = {}
#results_dict = {}
#stats_dict = {}
#with open(PERSISTENT_RESULTS_DIR + "dist9.400_size500_max16_rts0_dur5.0000_intv100_1_1_params.pkl", "rb") as handle:
#    params_dict = pickle.load(handle)
#with open(PERSISTENT_RESULTS_DIR + "dist9.400_size500_max16_rts0_dur5.0000_intv100_1_1_results.pkl", "rb") as handle:
#    results_dict = pickle.load(handle)
#with open(PERSISTENT_RESULTS_DIR + "dist9.400_size500_max16_rts0_dur5.0000_intv100_1_1_stats.pkl", "rb") as handle:
#    stats_dict = pickle.load(handle)

params_dict = {}
params_dict['seed'] = 1
params_dict['run'] = 1
params_dict['simDuration'] = 5.0
params_dict['packetArrivalRate'] = 100
params_dict['maxMpduCount'] = 16
params_dict['distance'] = 9.4
params_dict['mpduSize'] = 500
params_dict['rtsEnabled'] = 0

results_dict = {}
results_dict['perAvg'] = 0.07
results_dict['perStdDev'] = 0.001
results_dict['delayAvg'] = 0.000004
results_dict['delayStdDev'] = 0.000002
results_dict['delayMin'] = 0.0000001
results_dict['delayMax'] = 0.00003
results_dict['aggregationCountAvg'] = 12.4
results_dict['aggregationCountStdDev'] = 5.6
results_dict['aggregationCountMin'] = 1
results_dict['aggregationCountMax'] = 16
results_dict['mdpuCount'] = 789012
results_dict['mdpuDropCount'] = 234
results_dict['throughput'] = 123.45

stats_dict = {}
stats_dict['start'] = 132456780
stats_dict['duration'] = 9

exp_uploader.publish_to_sheets(params_dict, results_dict, stats_dict)