#!/usr/bin/env python3

import sys
import pickle

if len(sys.argv) > 1:
    for arg in sys.argv[1:]:
        try:
            with open(str(arg), "rb") as handle:
                print(pickle.load(handle))
        except:
            print("Unable to open >>%s<<" % str(arg))
