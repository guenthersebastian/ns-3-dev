import subprocess

for distance in [0.0, 4.0, 8.0, 9.0, 9.2, 9.4, 9.6, 9.8, 10.0, 10.2, 10.4, 10.6, 10.8, 11.0, 11.2, 11.4]:
    for mpdu_size in [100, 200, 500, 1000, 1500]:
            command = "screen -dm bash -c \"python run_runs.py -z 1 -t 5.0 -i 100 -d %.03f -s %d -a 64 -e 0\"" % (distance, mpdu_size)
            print(command)
            subprocess.call(command, shell=True)
