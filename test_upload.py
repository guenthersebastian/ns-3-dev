import exp_uploader
import pickle

PERSISTENT_RESULTS_DIR = "/home/seb/repos/ns-3-dev/results_remote/"
distances = [0.0, 4.0, 8.0, 9.0, 9.2, 9.4, 9.6, 9.8, 10.0, 10.2, 10.4, 10.6, 10.8, 11.0, 11.2, 11.4]

for distance in distances:
    for aggr_limit in [16, 32, 64, 256]:
        for run in range(1, 6, 1):
            params_dict = {}
            results_dict = {}
            stats_dict = {}
            run_name = "dist%.03f_size500_max%d_rts0_dur5.0000_intv100_1_%d" % (distance, aggr_limit, run)
            try:
                with open(PERSISTENT_RESULTS_DIR + run_name + "_params.pkl", "rb") as handle:
                    params_dict = pickle.load(handle)
                with open(PERSISTENT_RESULTS_DIR + run_name + "_results.pkl", "rb") as handle:
                    results_dict = pickle.load(handle)
                with open(PERSISTENT_RESULTS_DIR + run_name + "_stats.pkl", "rb") as handle:
                    stats_dict = pickle.load(handle)
                exp_uploader.publish_to_sheets(params_dict, results_dict, stats_dict)
            except:
                pass