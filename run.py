import argparse

import exp_mgmt
import exp_uploader

# argument parsing
parser = argparse.ArgumentParser(description='ns3 simulation runner')
parser.add_argument('-z', '--seed', type=int, default=1, dest='seed', help='the seed for the simulation')
parser.add_argument('-r', '--run', type=int, default=1, dest='run', help='the run number for the simulation')
parser.add_argument('-t', '--simDuration', type=float, default=0.01, dest='simDuration', help='the runtime of the simulation')
parser.add_argument('-i', '--packetArrivalRate', type=int, default=100, dest='packetArrivalRate', help='the interval of packet arrivals in ns')
parser.add_argument('-d', '--distance', type=float, default=10.0, dest='distance', help='the distance between STA and AP')
parser.add_argument('-s', '--mpduSize', type=int, default=500, dest='mpduSize', help='the size of the MPDUs exchanged')
parser.add_argument('-a', '--mpduAggregationLimit', type=int, default=64, choices=[16, 32, 64, 256], dest='mpduAggregationLimit', help='maximum amount of MPDUs to be aggregated')
parser.add_argument('-e', '--rtsEnabled', type=int, default=0, choices=[0, 1], dest='rtsEnabled', help='1 to enable RTS/CTS, 0 to disable')
parser.add_argument('-m', '--mcsIndex', type=int, default=7, choices=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], dest='mcsIndex', help='MCS index for data mode')

args = parser.parse_args()
sim_params = {}
sim_params['seed'] = args.seed
sim_params['run'] = args.run
sim_params['simDuration'] = args.simDuration
sim_params['packetArrivalRate'] = args.packetArrivalRate
sim_params['maxMpduCount'] = args.mpduAggregationLimit
sim_params['distance'] = args.distance
sim_params['mpduSize'] = args.mpduSize
sim_params['rtsEnabled'] = args.rtsEnabled
sim_params['mcsIndex'] = "HeMcs%d" % args.mcsIndex

sim_stats = exp_mgmt.run_experiment(sim_params)
sim_results = exp_mgmt.aggregate_results(sim_params)
exp_mgmt.move_results(sim_params)
exp_uploader.publish_to_sheets(sim_params, sim_results, sim_stats)
