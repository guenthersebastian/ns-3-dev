import pickle
import os
import time
import re
import subprocess

import exp_log_parser

RESULT_FILE_DIR = "/home/seb/logdir/"
PERMANENT_LOG_DIR = "/home/seb/exp/results/"

# compiles a string that contains all input parameters
def get_run_name(params_dict):
    return "dist%.03f_size%d_max%d_rts%d_dur%.04f_intv%d_%d_%d" % (
        params_dict['distance'], params_dict['mpduSize'], params_dict['maxMpduCount'], params_dict['rtsEnabled'], 
        params_dict['simDuration'], params_dict['packetArrivalRate'], params_dict['seed'], params_dict['run'])


# write a variable to a file
def save_var_to_file(file_name, variable):
    with open(file_name, 'wb') as handle:
        pickle.dump(variable, handle)


# write the supplied parameters to a file
def save_params_file(params_dict, output_dir=RESULT_FILE_DIR):
    run_name = get_run_name(params_dict)
    save_var_to_file("%s/%s_params.pkl" % (output_dir, run_name), params_dict)


# parses the out file and extracts the throughput value
def process_out_file(out_file_name):
    print("PARSING out file: %s" % out_file_name)
    throughput = 0.0
    with open(out_file_name) as out_file:
        throughput_pattern = re.compile('Throughput with default configuration \(distance: ([0-9\.]*), A-MPDU aggregation enabled, ([0-9]*)kB\): ([0-9\.]*) Mbit\/s, lost: ([0-9]*)')
        for line in out_file:
            throughput_line = throughput_pattern.search(line.strip())
            if throughput_line != None:
                throughput = float(throughput_line.group(3))
    return throughput


# parses the logfiles and saves an aggregated result
def aggregate_results(params_dict, log_dir=RESULT_FILE_DIR):
    # setup
    run_name = get_run_name(params_dict)
    
    # parse log file
    exp_results = exp_log_parser.process_log_file("%s%s_log.txt" % (log_dir, run_name), params_dict)

    # parse out file
    exp_results['throughput'] = process_out_file("%s%s_out.txt" % (log_dir, run_name))

    # save results
    print("RESULT: " + str(exp_results))
    save_var_to_file("%s%s_results.pkl" % (log_dir, run_name), exp_results)
    return exp_results


# runs an experiment and saves parameters, logs and duration
def run_experiment(params_dict):
    print("PARAMS: " + str(params_dict))
    save_params_file(params_dict)
    run_name = get_run_name(params_dict)
    log_path = RESULT_FILE_DIR + run_name
    ns3_path = "/home/seb/exp/ns%d/" % params_dict['maxMpduCount']
    stderr_file = open(log_path + "_log.txt", "w")
    stdout_file = open(log_path + "_out.txt", "w")

    # set up the command
    # command = "cd ./ns%d/; ./waf --run \"wifi-cback --distance=%.01f --enablePcap=0 --enableRts=%d --simulationTime=%.01f --payloadSize=%d --RngSeed=%d --RngRun=%d\" 2>%s_log.txt >%s_out.txt" % (params_dict['maxMpduCount'], params_dict['distance'], params_dict['rtsEnabled'], params_dict['simDuration'], params_dict['mpduSize'], params_dict['seed'], params_dict['run'], log_path, log_path)
    command = "./waf --run \"wifi-cback --distance=%.01f --enablePcap=0 --enableRts=%d --simulationTime=%.03f --packetArrivalTime='%dns' --payloadSize=%d --RngSeed=%d --RngRun=%d --aggregationLimit=%d --dataModeMcsIndex=%s\"" % (params_dict['distance'], params_dict['rtsEnabled'], params_dict['simDuration'], params_dict['packetArrivalRate'], params_dict['mpduSize'] - 70, params_dict['seed'], params_dict['run'], params_dict['maxMpduCount'], params_dict['mcsIndex'])
    print("RUN: %s" % command)
    time_start = time.time()

    # add logging vars to environment
    environment_variables = os.environ.copy()
    environment_variables["NS_LOG"] = "MacLow=level_debug|time:WifiPhy=level_debug|time:PropagationLossModel=level_debug|time:UdpServer=level_info|time"
    # print("Environment: " + str(os.environ))

    # execute the simulation
    # os.system(command)
    subprocess.call(command, stdout=stdout_file, stderr=stderr_file, cwd=ns3_path, env=environment_variables, shell=True)

    # collect timing stats
    time_duration = time.time() - time_start
    stats = {"start": time_start, "duration": time_duration}
    save_var_to_file("%s%s_stats.pkl" % (RESULT_FILE_DIR, run_name), stats)
    print("DURATION: %ds" % time_duration)
    return stats


def move_results(params_dict):
    # move all files from SSD to HDD
    command_mv = "mv %s* %s" % (RESULT_FILE_DIR + get_run_name(params_dict), PERMANENT_LOG_DIR)
    print("MOVING: %s" % command_mv)
    subprocess.call(command_mv, shell=True)
