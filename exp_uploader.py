import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


# upload params, results and stats to Sheets
def publish_to_sheets(params_dict, results_dict, stats_dict):

    SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
    NS3_SPREADSHEET_ID = '1RQEinDf6_yNk9DWG4Dq3W2LFgjmDbvYlfb3vcCQ3R28'
    NS3_DATA_RANGE_NAME = 'runs!A2:W'

    # authentication
    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    
    # log in if necessary
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    service = build('sheets', 'v4', credentials=creds)

    values = []
    if 'FAULTY_RESULTS' in results_dict:
        print("Results contain errors")
        values = [[
            params_dict['seed'],
            params_dict['run'],
            params_dict['simDuration'],
            params_dict['packetArrivalRate'],
            params_dict['maxMpduCount'],
            params_dict['distance'],
            params_dict['mpduSize'],
            params_dict['rtsEnabled'],
            results_dict['throughput'],
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            results_dict['mdpuCount'],
            results_dict['mdpuDropCount'],
            stats_dict['start'],
            stats_dict['duration']
        ]]
    else:    
        values = [[
            params_dict['seed'],
            params_dict['run'],
            params_dict['simDuration'],
            params_dict['packetArrivalRate'],
            params_dict['maxMpduCount'],
            params_dict['distance'],
            params_dict['mpduSize'],
            params_dict['rtsEnabled'],
            results_dict['throughput'],
            results_dict['perAvg'],
            results_dict['perStdDev'],
            results_dict['delayAvg'],
            results_dict['delayStdDev'],
            results_dict['delayMin'],
            results_dict['delayMax'],
            results_dict['aggregationCountAvg'],
            results_dict['aggregationCountStdDev'],
            results_dict['aggregationCountMin'],
            results_dict['aggregationCountMax'],
            results_dict['mdpuCount'],
            results_dict['mdpuDropCount'],
            stats_dict['start'],
            stats_dict['duration']
        ]]
        
    body = {'values': values}
    result = service.spreadsheets().values().append(spreadsheetId=NS3_SPREADSHEET_ID, range=NS3_DATA_RANGE_NAME, valueInputOption="RAW", body=body).execute()
    print('{0} cells appended.'.format(result.get('updates').get('updatedCells')))
