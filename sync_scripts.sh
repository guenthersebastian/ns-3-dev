#!/bin/bash

# EXP framework files
scp exp_*.py rvs27:exp/
# RUN scripts
scp run.py rvs27:exp/
scp run_runs.py rvs27:exp/
scp run_all_*.py rvs27:exp/
# FIX scripts
scp fix_*.py rvs27:exp/
# SYNC scripts
scp sync_*.sh rvs27:exp/
# UPLOAD scripts
scp upload_*.py rvs27:exp/
# DONT DO PLOTS