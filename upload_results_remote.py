#!/usr/bin/env python3

import exp_frmwrk
import exp_frmwrk_upload
import pickle


rows = []

for result_set in exp_frmwrk.get_all_pkl_sets("/home/seb/repos/ns-3-dev/results_remote/"):
    try:
        with open(result_set[0], "rb") as handle:
            params_dict = pickle.load(handle)
        with open(result_set[1], "rb") as handle:
            results_dict = pickle.load(handle)
        with open(result_set[2], "rb") as handle:
            stats_dict = pickle.load(handle)
        rows.append(exp_frmwrk_upload.create_row(params_dict, results_dict, stats_dict))
    except Exception as e:
        print("ERROR with set: %s (%s)" % (result_set[0], str(e)))

print("UPLOADING...")
added = exp_frmwrk_upload.upload_rows(rows)
print("appended %d cells (%.01f rows)" % (added, added / 23))