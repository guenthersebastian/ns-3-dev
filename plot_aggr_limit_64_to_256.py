#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle


distances = [0.0, 4.0, 8.0, 9.0, 9.2, 9.4, 9.6, 9.8, 10.0, 10.2, 10.4, 10.6, 10.8, 11.0, 11.2, 11.4]

df_errorrate_goodput = pd.DataFrame(columns=("distance", "limit", "goodput"))

for distance in distances:
    for aggr_limit in [64, 256]:
        for run in range(1, 6, 1):
            result_filename = "/home/seb/repos/ns-3-dev/results_remote/dist%.03f_size500_max%d_rts0_dur5.0000_intv100_1_%d_results.pkl" % (distance, aggr_limit, run)
            try:
                sim_results_file = open(result_filename, "rb")
                sim_results = pickle.load(sim_results_file)
                sim_results_file.close()
                df_errorrate_goodput.loc[len(df_errorrate_goodput.index)] = [distance, 'limit%d' % aggr_limit, sim_results['throughput']]
                print("Loaded: %s" % result_filename)
            except IOError:
                print("Does not exist: %s" % result_filename)

print(df_errorrate_goodput.to_string())

# get mean and standard distribution
mean_errorrate_goodput_64 = df_errorrate_goodput.where(df_errorrate_goodput['limit']=='limit64').groupby('distance')['goodput'].mean()
std_errorrate_goodput_64 = df_errorrate_goodput.where(df_errorrate_goodput['limit']=='limit64').groupby('distance')['goodput'].std()
mean_errorrate_goodput_256 = df_errorrate_goodput.where(df_errorrate_goodput['limit']=='limit256').groupby('distance')['goodput'].mean()
std_errorrate_goodput_256 = df_errorrate_goodput.where(df_errorrate_goodput['limit']=='limit256').groupby('distance')['goodput'].std()

# plot it
plt.figure(figsize=(10.0, 5.0))
plt.errorbar(mean_errorrate_goodput_64.index, mean_errorrate_goodput_64, xerr=None, yerr=2*std_errorrate_goodput_64, linestyle=':', fmt='.r', markersize=1, barsabove=True, capsize=2, ecolor="r")
plt.errorbar(mean_errorrate_goodput_256.index, mean_errorrate_goodput_256, xerr=None, yerr=2*std_errorrate_goodput_256, linestyle=':', fmt='.g', markersize=1, barsabove=True, capsize=2, ecolor="g")
plt.legend(labels=("aggregation limit 64", "aggregation limit 256"), loc=3, fancybox=False, edgecolor="k")
plt.xlabel('Distance (m)')
plt.xticks(np.arange(0.0, 11.6, 0.5))
plt.xlim([-0.1, 11.6])
plt.ylabel('Goodput (Mbit/s)')
#plt.ylim([0, 1300000000])
plt.savefig("img_paper_errorrate-goodput-aggr-limit-64-256.png", format="png", dpi=300)
if __name__ == '__main__':
    plt.show()
