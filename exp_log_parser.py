import re
import statistics
import collections


# parses the log file and extracts various stats
def process_log_file(log_file_name, params_dict):
    print("PARSING log file: %s" % log_file_name)

    exp_results = {}

    # MPDU aggregation counts
    mpdu_counts = []
    # DELAYs
    delays = []
    # PERs
    frame_error_rates = []
    # DROPS
    dropped_mpdus_count = 0

    # set up patterns
    pattern_ampdu = re.compile('\+([0-9\.]*)s \[mac=00:00:00:00:00:02\] tx unicast A-MPDU containing ([0-9]*) MPDUs')
    pattern_smpdu = re.compile('\+([0-9\.]*)s \[mac=00:00:00:00:00:02\] tx unicast S-MPDU with sequence number ([0-9]*)')
   
    pattern_send = re.compile('\+([0-9\.]*)s \[mac=00:00:00:00:00:02\] Adding packet with sequence number ([0-9]*) to A-MPDU, packet size = ([0-9]*), A-MPDU size = ([0-9]*)')
    pattern_receive = re.compile('\+([0-9\.]*)s \[mac=00:00:00:00:00:01\] Deaggregate packet from 00:00:00:00:00:02 with sequence=([0-9]*)')
    pattern_frame_error_rate = re.compile('\+([0-9\.]*)s mode=([0-9]*), snr\(dB\)=([0-9\.]*), per=([0-9e\.\-]*), size=' + str(params_dict['mpduSize']))

    # parse line by line
    parser_state = 0    # 0 collect send times; 2 sending done, receive
    currently_sent_mpdus = {}
    expected_retransmissions = []
    line_number = 0
    with open(log_file_name) as log_file:
        for line in log_file:
            # always check frame error rates
            search_frame_error_rate = pattern_frame_error_rate.search(line.strip())
            if search_frame_error_rate != None:
                frame_error_rates.append(float(search_frame_error_rate.group(4)))

            # always collect aggregation counts
            search_ampdu = pattern_ampdu.search(line.strip())
            if search_ampdu != None:
                mpdu_counts.append(int(search_ampdu.group(2)))
                parser_state = 1
            search_smpdu = pattern_smpdu.search(line.strip())
            if search_smpdu != None:
                mpdu_counts.append(1)
                parser_state = 1

            # state just transitioned
            if parser_state == 1:
                # print("Transmitting: " + str(currently_sent_mpdus))
                for key in expected_retransmissions:
                    if key not in currently_sent_mpdus:
                        print("Dropping %s (line %d)" % (key, line_number))
                        dropped_mpdus_count += 1
                parser_state = 2

            # check if a new cycle has started
            search_send = pattern_send.search(line.strip())
            if (parser_state == 2) and (search_send != None):
                parser_state = 0
                expected_retransmissions = []
                for key in currently_sent_mpdus:
                    expected_retransmissions.append(key)
                currently_sent_mpdus = {}
                # print("Errors: " + str(expected_retransmissions))

            # state machine
            if parser_state == 0:
                # collect sent MPDUs
                if search_send != None:
                    seq = search_send.group(2)
                    if seq not in currently_sent_mpdus:
                        currently_sent_mpdus[seq] = float(search_send.group(1))
            elif parser_state == 2:
                # collect received MDPUs
                search_receive = pattern_receive.search(line.strip())
                if search_receive != None:
                    seq = search_receive.group(2)
                    if seq in currently_sent_mpdus:
                        delays.append(float(search_receive.group(1)) - currently_sent_mpdus[seq])
                        currently_sent_mpdus.pop(seq)
                    else:
                        print("Not sent, but received: %s" % seq)

            line_number += 1

    # calculate statistics
    try:
        exp_results['perAvg'] = statistics.mean(frame_error_rates)
        exp_results['perStdDev'] = statistics.stdev(frame_error_rates)
    except:
        exp_results['FAULTY_RESULTS'] = True
        print("Error while gathering frame_error_rates")
    
    try:
        exp_results['delayAvg'] = statistics.mean(delays)
        exp_results['delayStdDev'] = statistics.stdev(delays)
        exp_results['delayMin'] = min(delays)
        exp_results['delayMax'] = max(delays)
    except:
        exp_results['FAULTY_RESULTS'] = True
        print("Error while gathering delays")
    
    try:
        exp_results['aggregationCountAvg'] = statistics.mean(mpdu_counts)
        exp_results['aggregationCountStdDev'] = statistics.stdev(mpdu_counts)
        exp_results['aggregationCountMin'] = min(mpdu_counts)
        exp_results['aggregationCountMax'] = max(mpdu_counts)
    except:
        exp_results['FAULTY_RESULTS'] = True
        print("Error while gathering mpdu_counts")

    exp_results['mdpuCount'] = len(delays)
    exp_results['mdpuDropCount'] = dropped_mpdus_count

    return exp_results
