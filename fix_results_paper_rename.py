#!/usr/bin/env python3

import os
import re


RESULTS_DIR = "/home/seb/repos/ns-3-dev/results_paper/"
FILENAME_PATTERN = re.compile("ergoodput-dist([0-9.]+)-size([0-9]+)-run([0-9]+)(|-rtscts).txt")

for file in os.listdir(RESULTS_DIR):
    if file.endswith(".txt"):
        file_name = os.path.join(RESULTS_DIR, file)
        filename_matches = FILENAME_PATTERN.search(str(file).strip())
        if filename_matches == None:
            print("Skipping: %s" % str(file))
        else:
            distance = filename_matches.group(1)
            size = filename_matches.group(2)
            run = filename_matches.group(3)
            rts_enabled = 0
            if filename_matches.group(4) == "-rtscts":
                rts_enabled = 1
            old_log_filename = "ergoodput-dist%s-size%s-run%s%s.txt" % (distance, size, run, filename_matches.group(4))
            old_out_filename = "ergoodput-dist%s-size%s-run%s%s_out.txt" % (distance, size, run, filename_matches.group(4))
            new_log_filename = "dist%s00_size%s_max64_rts%d_dur5.0000_intv100_1_%s_log.txt" % (distance, size, rts_enabled, run)
            new_out_filename = "dist%s00_size%s_max64_rts%d_dur5.0000_intv100_1_%s_out.txt" % (distance, size, rts_enabled, run)
            os.rename(os.path.join(RESULTS_DIR, old_log_filename), os.path.join(RESULTS_DIR, new_log_filename))
            os.rename(os.path.join(RESULTS_DIR, old_out_filename), os.path.join(RESULTS_DIR, new_out_filename))
